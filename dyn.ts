import axios from 'axios';
import * as xmljs from 'xml-js'

export interface IDynadotServiceSearchResult {
    domain: string,
    available: boolean,
    price: number,
    currency: string
  }

  export interface IDynadotSearchHeader {
    Available: {
      _text: 'yes' | 'no'
    },
    DomainName: {
      _text: string
    },
    Price: {
      _text: string
    },
    Status: {
      _text: 'success' | string
    },
    SuccessCode: {
      _text: '0' | '-1' // 0 - success, -1  - error
    }
  }
 
  export interface IDynadotSearchResults { 
    Results: {
      SearchResponse: {SearchHeader: IDynadotSearchHeader} // не нашел массивов в объекте data
    }
  }

  export interface IDynadotRegisterHeader {
    SuccessCode: {
      _text: string
    },
    Status: {
      _text: string
    }
  }

  export interface IDynadotRegisterResults {
    RegisterResponse: {
      RegisterHeader: IDynadotRegisterHeader
    }
  }

  export interface ISetNS {
    domainName: string,
    ns1: string,
    ns2: string,
  }
 
  export class DynadotAPI {
    private apiKey: string;
    constructor( {apiKey: apiKey} )
    { this.apiKey = apiKey }
  
 
  async checkAvailabilityAndPrice (domain: string[]): Promise<IDynadotServiceSearchResult[]> {
    let arrayOfResponses = [];
    for (let i = 0; i<domain.length; i++) { // если у нас не bulk и не супер bulk, идет отдельный запрос по каждому домену
      const dummyResponse = {
        domain:domain[i],
        available: false,
        price: null,
        currency: 'USD'
      };
      let ddResult;
      try {
        ddResult = await axios.get('https://api.dynadot.com/api3.xml', {
          params: {
            key: this.apiKey,
            command: 'search',
            domain0: domain[i],
            "show_price": 1,
            currency: 'USD'
          }
        });
      } catch (e) {
        console.log(e);
        arrayOfResponses.push(dummyResponse);
      }
      const data = xmljs.xml2js(ddResult.data, { compact: true }) as IDynadotSearchResults; 
      // проверка  if (data.Results.SearchResponse.length === 0) return dummyResponse; не работает
      const info = data.Results.SearchResponse.SearchHeader; //data.Results.SearchResponse.SearchHeader без [0] работает
      if (info.SuccessCode._text !== '0' || info.Available._text === 'no') { // 0 - success, -1  - error
        arrayOfResponses.push(dummyResponse); 
        continue
      } 
      const [price, currency] = info.Price._text.split(' in ');
      const result = {
        domain: info.DomainName._text,
        price: parseFloat(price),
        currency,
        available: info.Available._text === 'yes'
      };
  
      arrayOfResponses.push(result);
    }
    return arrayOfResponses;
  }

   async registerDomains(domains: string[]): Promise<IDynadotRegisterHeader[]> {
    let arrayOfResponses = [];
    for (let i = 0; i < domains.length; i++) {
    const ddResult = await axios.get('https://api.dynadot.com/api3.xml', {
      params: {
        key: this.apiKey,
        command: 'register',
        domain: domains[i],
        duration: 1,
        currency: 'USD'
      }
    });
    // если не хотим ничего возваращать - все ниже можно убрать
      const data = xmljs.xml2js(ddResult.data, { compact: true }) as IDynadotRegisterResults;
      const info = data.RegisterResponse.RegisterHeader;
      arrayOfResponses.push({ successCode: info.SuccessCode._text, status: info.Status._text });
      }
    return arrayOfResponses
    }

    async changeNs( options: ISetNS ) { // можно добавлять NS-ки для нескольких доменов сразу, но тут только для одного
      options.ns1 = options.ns1.toLowerCase() //NS must be lowercase
      options.ns2 = options.ns2.toLowerCase()
      await axios.get('https://api.dynadot.com/api3.xml', {
      params: {
        key: this.apiKey,
        command: 'set_ns',
        domain: options.domainName,
        ns0: options.ns1,
        ns1: options.ns2
      }
    });
    }

  }

//   usage
//   (async () => {
//     const apiDD = new DynadotAPI({apiKey: '1111111111AAAAAAAAAAAA'})
//     console.log('result avail:', await apiDD.checkAvailabilityAndPrice(['topitemsworldwide.xyz', 'topshopww.xyz']))
//     console.log('result reg:', await apiDD.registerDomains(['google.com', 'topitemsworldwide.xyz', 'topshopww.xyz']))
//     await apiDD.changeNs({ domainName: 'topitemsworldwide.xyz', ns1: 'NS.VICTORIACF.COM', ns2: 'NS.NINE.COM'})
//   })()
  
